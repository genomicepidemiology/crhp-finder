/* Philip T.L.C. Clausen Jul 2017 s123580@student.dtu.dk */

/*
 Copyright (c) 2017, Philip Clausen, Technical University of Denmark
 All rights reserved.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
#define _XOPEN_SOURCE 600
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "pherror.h"
#include "qseqs.h"

struct genePos {
	char *name;
	int *pos;
	int numPos;
};

int fget2(Qseqs *line, FILE *file, char stopChar) {
	
	int c, len, size;
	unsigned char *ptr;
	
	len = 0;
	size = line->size;
	ptr = line->seq;
	while((c = fgetc(file)) != EOF) {
		if(++len == size) {
			if(!(line->seq = realloc(line->seq, (size <<= 1)))) {
				ERROR();
			} else {
				ptr = line->seq + len;
				line->size = size;
			}
		}
		if((*ptr++ = c) == stopChar) {
			*--ptr = 0;
			line->len = --len;
			return 1;
		}
	}
	*ptr = 0;
	line->len = len;
	
	return 0;
}

char * stripSpace(const unsigned char *src) {
	
	char *ptr;
	
	ptr = (char *) src;
	while(*ptr != 0 && isspace(*ptr)) {
		++ptr;
	}
	
	return ptr;
}

#define skipLine(file, c, stopChar) while((c = fgetc(file)) != EOF && c != stopChar)

int int_cmp(const void * a, const void * b) {
   return (*(int*)a - *(int*)b);
}

char * strsep(char **src, const char delim) {
	
	char *org, *ptr;
	
	org = *src;
	ptr = org;
	while(*ptr != delim) {
		++ptr;
	}
	*ptr++ = 0;
	*src = ptr;
	
	return org;
}

void helpMessage(int exeStatus) {
	FILE *helpOut;
	if(exeStatus == 0) {
		helpOut = stdout;
	} else {
		helpOut = stderr;
	}
	fprintf(helpOut, "# getGene finds the bascalls from a KMA mat file at specified positions.\n");
	fprintf(helpOut, "# Options are:\t\tDesc:\t\t\t\tDefault:\tRequirements:\n");
	fprintf(helpOut, "#\n");
	fprintf(helpOut, "#\t-pos\t\tDescription file\t\tNone\t\tREQUIRED\n");
	fprintf(helpOut, "#\t-i\t\tKMA output\t\t\tNone\t\tREQUIRED\n");
	fprintf(helpOut, "#\t-h\t\tShows this help message\n");
	fprintf(helpOut, "#\n");
	exit(exeStatus);
}

int main(int argc, char *argv[]) {
	
	int i, j, k, args, thisPos, wanted_max, wanted_count, want, parsed_count;
	int nucs[6];
	double tot;
	char *line_sep, *inputfilename, *inputfilenames, *err, **parsedTemplates;
	FILE *inputfile, *desc_file;
	Qseqs *line;
	struct genePos *wanted, *thisWanted;
	for(i = 0; i < 6; i++) {
		nucs[i] = 0;
	}
	
	/* SET DEFAULTS */
	inputfile = 0;
	desc_file = 0;
	inputfilename = 0;
	inputfilenames = 0;
	line = setQseqs(256);
	
	/* PARSE COMMAND LINE OPTIONS */
	args = 1;
	while(args < argc) {
		if(strcmp(argv[args], "-pos") == 0) {
			args++;
			if(args < argc) {
				desc_file = sfopen(argv[args], "r");
			}
		} else if(strcmp(argv[args], "-i") == 0) {
			args++;
			if(args < argc) {
				inputfilenames = smalloc(strlen(argv[args]) + strlen("gunzip -c .mat.gz") + 1);
				inputfilename = strdup(argv[args]);
			}
		} else if(strcmp(argv[args], "-h") == 0) {
			helpMessage(0);
		} else {
			fprintf(stderr, "# Invalid option:\t%s\n", argv[args]);
			fprintf(stderr, "# Printing help message:\n");
			helpMessage(-1);
		}
		args++;
	}
	
	if(desc_file == 0 || inputfilename == 0) {
		fprintf(stderr, "# Too few arguments handed\n");
		fprintf(stderr, "# Printing help message:\n");
		helpMessage(-1);
	}
	
	/* parse result file */
	sprintf(inputfilenames, "%s.res", inputfilename);
	inputfile = sfopen(inputfilenames, "r");
	parsed_count = 0;
	wanted_max = 16;
	parsedTemplates = smalloc(wanted_max * sizeof(char*));
	
	fget2(line, inputfile, '\n');
	if(*line->seq != '#') {
		fprintf(stderr, "Corrupted file: %s\n", inputfilenames);
		exit(1);
	}
	while(!feof(inputfile)) {
		fget2(line, inputfile, '\t');
		if(line->len) {
			parsedTemplates[parsed_count] = strdup((char *) line->seq);
			if(!parsedTemplates[parsed_count]) {
				ERROR();
			}
			skipLine(inputfile, args, '\n');
			if(++parsed_count == wanted_max) { // realloc genes
				wanted_max *= 2;
				parsedTemplates = realloc(parsedTemplates, wanted_max * sizeof(char*));
				if(!parsedTemplates) {
					ERROR();
				}
			}
		}
	}
	fclose(inputfile);
	
	wanted = smalloc((parsed_count + 1) * sizeof(struct genePos));
	wanted_count = 0;
	/* parse desc file */
	while(!feof(desc_file)) {
		fget2(line, desc_file, '\n');
		if(line->len > 0) {
			/* get number of positions */
			wanted[wanted_count].numPos = 0;
			line_sep = (char*)(line->seq);
			while(*line_sep) {
				if(*line_sep++ == '\t') {
					wanted[wanted_count].numPos++;
				}
			}
			
			/* get name */
			line_sep = (char*)(line->seq);
			wanted[wanted_count].name = strdup(strsep(&line_sep, '\t'));
			
			want = 0;
			for(i = 0; i < parsed_count && !want; i++) {
				if(strcmp(wanted[wanted_count].name, parsedTemplates[i]) == 0) {
					*(parsedTemplates[i]) = 0;
					want = 1;
				}
			}
			if(want) {
				/* get positions */
				wanted[wanted_count].pos = smalloc(wanted[wanted_count].numPos * sizeof(int));
				for(i = 0; i < wanted[wanted_count].numPos; i++) {
					wanted[wanted_count].pos[i] = strtod(strsep(&line_sep, '\t'), &err);
					if(*err != 0) {
						fprintf(stderr, "%s\n", err);
						exit(1);
					}
				}
				
				/* sort numbers */
				qsort(wanted[wanted_count].pos, wanted[wanted_count].numPos, sizeof(int), int_cmp);
				wanted_count++;
			} else {
				free(wanted[wanted_count].name);
			}
		}
	}
	fclose(desc_file);
	
	/* parse mat-file */
	sprintf(inputfilenames, "gunzip -c %s.mat.gz", inputfilename);
	inputfile = popen(inputfilenames, "r");
	if(!inputfile) {
		fprintf(stderr, "No such file or directory:\t%s\n", argv[args]);
	}
	while(!feof(inputfile)) {
		fget2(line, inputfile, '\n');
		if(*line->seq == '#') { // new template
			/* check if this template is wanted */
			thisWanted = 0;
			for(i = 0; i < wanted_count && thisWanted == 0; i++) {
				if(strcmp((char *) line->seq + 1, wanted[i].name) == 0) {
					thisWanted = &wanted[i];
				}
			}
			/* parse positions */
			if(thisWanted) {
				fprintf(stdout, "%s\t", thisWanted->name);
				i = 0;
				j = 0;
				thisPos = thisWanted->pos[j];
				while(!feof(inputfile)) {
					fget2(line, inputfile, '\n');
					if(*line->seq != '-') {
						i++;
					}
					if(i == thisPos && line->len != 0) {
						fprintf(stdout, "%d\t%s", thisPos, line->seq);
						/* extract percentages */
						line_sep = (char *) (line->seq);
						while(*line_sep++ != '\t');
						tot = 0;
						for(k = 0; k < 5; k++) {
							nucs[k] = strtod(strsep(&line_sep, '\t'), &err);
							if(*err != 0) {
								fprintf(stderr, "%s\n", err);
								exit(1);
							}
							tot += nucs[k];
						}
						nucs[5] = strtod(line_sep, &err);
						if(*err != 0) {
							fprintf(stderr, "%s\n", err);
							exit(1);
						}
						tot += nucs[5];
						
						if(tot == 0) {
							for(k = 0; k < 5; k++) {
								fprintf(stdout, "\t%.2f", 0.00);
							}
							fprintf(stdout, "\t%.2f", 100.00);
						} else {
							for(k = 0; k < 6; k++) {
								fprintf(stdout, "\t%.2f", 100 * nucs[k] / tot);
							}
						}
						fprintf(stdout, "\n");
						j++;
						if(j >= thisWanted->numPos) {
							break;
						} else {
							thisPos = thisWanted->pos[j];
							fprintf(stdout, "%s\t", thisWanted->name);
						}
					}
					if(line->len == 0) {
						break;
					}
				}
			}
		}
	}
	pclose(inputfile);
	
	return 0;
}




