CFLAGS = -Wall -O3 -std=c99
BINS = getGene
LIBS = pherror.o qseqs.o

.c .o:
	$(CC) $(CFLAGS) -c -o $@ $<

all: $(BINS)

getGene: getGene.c $(LIBS)
	$(CC) $(CFLAGS) -o $@ $< $(LIBS)

clean:
	$(RM) $(BINS) $(LIBS)

pherror.o: pherror.h
qseqs.o: qseqs.h pherror.h
